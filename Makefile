clean:
	rm -rf ./.cache/
	find ./ -name "*.pyc" -exec rm -rf {} \; || true
	find ./ -name ".cache" -exec rm -rf {} \; || true
	find ./ -name "__pycache__" -exec rm -rf {} \; || true
	find ./ -name "*.py~" -exec rm -rf {} \; || true
	find ./ -name ".#*" -exec rm -rf {} \;
