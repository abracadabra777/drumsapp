# Архитектура

Все необходимые компоненты запускаются с помощью контейнеризации под управлением `docker` с помощью `docker-compose`.
Все контейнеры запускаются в рамках одной сети и видят друг друга по внутрисетевым ip адресам или хостнеймам.
Контейнер nginx пробрасывает свои порты 80 и 443 наружу для доступа из интернета.

Зависимости приложения устанавливаются при сборке `./services/app/Dockerfile.python`, код приложения монтируется в момент запуска контейнера.

 python приложение запускается с помощью uwsgi.
Конфиги для postgresql, nginx, uwsgi лежат в директории `./services/`.
Параметры запуска не тюнингованые.


## Что необходимо для запуска?
Предполагается что сервер чистый и на нем не установлены nginx и postgresql на хостмашине.

Для запуска необходимо установить `docker` и `docker-compose`.
Например по [инструкции](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru).

Для прода используем файл docker-compose.prod.yml
Для локальной разработки используем docker-compose.yml (не забываем указать правильные пути на хост машине)

### Чтобы все корректно работало


1. Собираем статику в директорию static
2. Мигрируем базу данных
3. Запускаем приложение, все должно быть доступно по нужным адресам


## Запуск для разработки локально:
```bash
docker-compose -f docker-compose.prod.yml up --build
```

Приложение должно быть доступно по адресу: http://127.0.0.1:80

## Запуск в продакшене:

Расположить по адресу `/usr/src/drumsapp`. Из директории с приложением выполнить команду:

```bash
docker-compose -f docker-compose.prod.yml up --build -d
```
Отвязывает запущенный пероцесс от текущего терминала.

На сервере приложение должно быть доступно по адресу http://127.0.0.1:80 и из интернета по ip и доменному имени.


## Пусть к статическим файлам

Статические факлы можно расположить где угодно на хост системе, но в docker-compose.yml и docker-compose.prod.yaml необходимо заменить соответствующий путь. Пути к файлам внутри контейнеров лучше не трогать.

Сборка статики django:

`docker-compose -f docker-compose.yaml run app python ./manage.py collectstatic`

`./static/admin` - статика админки
`./static/app` - статика приложения

## Персистентность базы данных
В проде для сохранения данных БД между перезапусками, файлы монтируются на хост систему в директорию `/var/lib/postgresql/data`.


## Накатка миграций БД
`docker-compose -f docker-compose.prod.yml run app python ./manage.py migrate`

## Создание суперпользователя админки
`docker-compose -f docker-compose.prod.yml run app python ./manage.py createsuperuser --help`

Вход по адресу http://127.0.0.1/admin
