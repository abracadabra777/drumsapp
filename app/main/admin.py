from django.contrib import admin
from .models import SubscriptionModel


class SubscriptionModelAdmin(admin.ModelAdmin):
    pass


admin.site.register(SubscriptionModel, SubscriptionModelAdmin)
