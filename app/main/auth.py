from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User
from django.conf import settings

import jwt
from jwt.exceptions import InvalidSignatureError, ExpiredSignatureError


class TokenBackend(BaseBackend):
    def authenticate(self, request, token):

        token = request.GET.get('token', None)

        try:
            data = jwt.decode(token, settings.AUTH_SECRET, algorithms=["HS256"])
        except InvalidSignatureError as e:
            print(e)
            return

        except ExpiredSignatureError as e:
            print(e)
            return
        except Exception as e:
            print(e)
            return

        return self.get_user(data['user_id'])

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
