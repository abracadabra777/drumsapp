from django.db import models
from django.conf import settings


class SubscriptionModel(models.Model):
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    expired_at = models.DateTimeField()
