from django.urls import path

from . import views

urlpatterns = [
    path('login', views.LoginView.as_view(), name='login'),
    path('token', views.TokenView.as_view(), name='token'),
    path('secret', views.OnlyLoggedInView.as_view(), name='logged_only'),
    path('', views.IndexView.as_view(), name='index'),
]
