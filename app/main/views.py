
import jwt
from jwt.exceptions import InvalidSignatureError


from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from datetime import timedelta, datetime, timezone
from django.http import HttpResponse
from django.views import View
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.conf import settings
from django import forms
from .models import SubscriptionModel



class LoginForm(forms.Form):
    email = forms.EmailField(label="Email")


class IndexView(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'landing.html', {}, status=200)


class OnlyLoggedInView(View):

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        # Тут у нас только залогиненный пользователь может быть

        # Выбираем активные подписки пользователя и делаем нужные проверки
        subscriptions = SubscriptionModel.objects.filter(user_id=request.user.id, expired_at__gt=datetime.now(tz=timezone.utc))

        return render(request, 'logged_only.html', {}, status=200)


class TokenView(View):
    def get(self, request, *args, **kwargs):

        user = authenticate(request=request, token=request.GET.get('token'))
        if user is not None:
            login(request, user)
            return render(request, 'auth_success.html', {'user': user}, status=200)
        else:
            return render(request, 'auth_error.html', {}, status=400)



class LoginView(View):

    # Для отрисовки страницы логина
    def get(self, request, *args, **kwargs):
        form = LoginForm()

        return render(request, 'login.html', {'form': form}, status=200)

    def post(self, request, *args, **kwargs):

        form = LoginForm(request.POST)

        if not form.is_valid():
            return render(request, 'login_error.html', {'form': form}, status=400)

        email = form.cleaned_data['email']
        # Достаем пользователя или создаем нового
        try:
            user = User.objects.get(email__exact=email)
        except ObjectDoesNotExist:
            user = User.objects.create_user(email.lower(), email.lower())

        # Перестает быть валидным в течении 30 минут
        data = {
            "exp": datetime.now(tz=timezone.utc) + timedelta(minutes=30),
            "user_id": user.id,
        }
        token = jwt.encode(data, settings.AUTH_SECRET, algorithm="HS256")

        message_body =  render_to_string('token_email.html', {'token': token})
        # Отправляем email с токеном
        # раскомментировать, если реквизииты корректные проставить
        # send_mail(
        #     'Тема письма',
        #     message_body,
        #     'from@iplaydrums.ru',
        #     [email],
        #     fail_silently=False,
        # )


        # Печатаем чтобы токен увидеть при тестировании в консоли
        print(message_body)

        return render(request, 'login_success.html', {'form': form}, status=400)
