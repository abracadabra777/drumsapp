import sys

# Импеортируем дефолтные настройки
try:
    from settings.default_settings import *
except ImportError as e:
    print(e)
    sys.exit(0)


# Далее переопределяем настройки реквизиты к БД, пути к статике

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-4#+-^s#fmzux5%z15=&7-h+5+gr@#*m0$xu9yu@9!cv3p_2*(@'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'drumsapp',
        'USER': 'username',
        'PASSWORD': 'secret_password',
        'HOST': 'db-host',
        'PORT': '5432',
    }
}


STATIC_ROOT = "/static"
MEDIA_ROOT = "/media"

# Указать SMTP настройки почтового ящика отправителя
EMAIL_HOST = ''
EMAIL_PORT = ''
EMAIL_HOST_USER = ''
EMAIL_USE_TLS = True

ALLOWED_HOSTS = ['iplaydrums.ru', 'localhost', '127.0.0.1']
DEBUG = True
DEFAULT_FROM_EMAIL = 'webmaster@example.com'

AUTH_SECRET = "secret"
